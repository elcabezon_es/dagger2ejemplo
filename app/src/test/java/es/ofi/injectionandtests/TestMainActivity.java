package es.ofi.injectionandtests;

import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

import es.ofi.dagger2ejemplo.BuildConfig;
import es.ofi.dagger2ejemplo.MainActivity;
import es.ofi.dagger2ejemplo.R;
import es.ofi.dagger2ejemplo.model.SegundaActivity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 19)
public class TestMainActivity extends Contexto {

    Button boton;
    TextView text;
    MainActivity activity;

    @Before
    public void crear_actividad_y_botones() throws Exception {

        activity = Robolectric.setupActivity(MainActivity.class);
        text = (TextView) activity.findViewById(R.id.user);
        boton = (Button) activity.findViewById(R.id.clickEjemplo);

    }

    @Test
    public void comprueba_que_la_inyeccion_se_hace_correctamente() throws Exception {
        assertEquals(persona.getNombre(), text.getText().toString());
    }

    @Test
    public void cuando_se_pulsa_el_boton_se_lanza_segunda_actividad() throws Exception {
        boton.performClick();
        Intent expectedIntent = new Intent(activity, SegundaActivity.class);
        assertThat(Shadows.shadowOf(activity).getNextStartedActivity()).isEqualTo(expectedIntent);

    }

}