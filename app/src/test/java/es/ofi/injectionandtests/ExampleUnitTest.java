package es.ofi.injectionandtests;

import android.widget.TextView;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import es.ofi.dagger2ejemplo.BuildConfig;
import es.ofi.dagger2ejemplo.MainActivity;
import es.ofi.dagger2ejemplo.R;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 19)
public class ExampleUnitTest extends Contexto{
    @Test
    public void usuario_en_text_view() throws Exception {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        TextView text = (TextView)activity.findViewById(R.id.user);
        assertEquals(persona.getNombre(), text.getText().toString());
    }
}