package es.ofi.dagger2ejemplotest;

import junit.framework.TestCase;

import org.junit.runner.RunWith;

import es.ofi.dagger2ejemplo.model.Persona;

/**
 * Created by albertot on 15/03/2016.
 */

public class PersonaTest extends TestCase {
    public void test_PersonaMenordeEdad(){
        Persona p = new Persona("Alberto",19);
        assertTrue(p.menorEdad());
    }

    public void test_PersonaMayordeEdad(){
        Persona p = new Persona("Alberto",19);
        assertFalse(p.menorEdad());
    }

}
