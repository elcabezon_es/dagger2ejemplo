package es.ofi.dagger2ejemplo;

import es.ofi.dagger2ejemplo.model.Persona;

/**
 * Created by albertot on 15/03/2016.
 */
public interface IServicio {
    boolean validateUser(String username, String password);
    Persona getUsuario();
}
