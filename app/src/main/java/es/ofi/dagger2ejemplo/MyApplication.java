package es.ofi.dagger2ejemplo;

import android.app.Application;


import es.ofi.dagger2ejemplo.commu.CommuComponent;
import es.ofi.dagger2ejemplo.commu.DaggerCommuComponent;


/**
 * Created by albertot on 15/03/2016.
 */
public class MyApplication extends Application {

    CommuComponent commuComponent;
    public static String a = "Alberto";
    @Override
    public void onCreate() {
        super.onCreate();
        commuComponent = DaggerCommuComponent.builder().build();
    }
    public CommuComponent getCommuComponent(){
        return commuComponent;
    }

}
