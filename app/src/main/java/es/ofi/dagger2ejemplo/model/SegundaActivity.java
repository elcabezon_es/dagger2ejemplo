package es.ofi.dagger2ejemplo.model;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import es.ofi.dagger2ejemplo.R;

public class SegundaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
    }
}
