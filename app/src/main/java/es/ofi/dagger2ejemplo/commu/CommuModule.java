package es.ofi.dagger2ejemplo.commu;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class CommuModule {
    @Provides
    @Singleton
    public CommuServiceRest getNetwork() {
        return new CommuServiceRest();
    }
    @Provides
    @Singleton
    public CommuFake getFakeApi() {
        return new CommuFake();
    }

}
