package es.ofi.dagger2ejemplo.commu;

import es.ofi.dagger2ejemplo.IServicio;
import es.ofi.dagger2ejemplo.model.Persona;

public class CommuFake implements IServicio{
    public boolean validateUser(String username, String password) {
        // imagine an actual network call here
        // for demo purpose return false in "real" life
        if (username == password) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Persona getUsuario() {
        return new Persona("aaa",15);
    }
}
