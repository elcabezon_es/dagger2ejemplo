package es.ofi.dagger2ejemplo.commu;


import es.ofi.dagger2ejemplo.IServicio;
import es.ofi.dagger2ejemplo.model.Persona;

public class CommuServiceRest implements IServicio {
    public boolean validateUser(String username, String password) {
        // imagine an actual network call here
        // for demo purpose return false in "real" life
        if (username == null || username.length() == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Persona getUsuario() {
        return null;
    }
}
