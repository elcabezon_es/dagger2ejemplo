package es.ofi.dagger2ejemplo.commu;

import javax.inject.Singleton;

import dagger.Component;
import es.ofi.dagger2ejemplo.MainActivity;

/**
 * Created by albertot on 15/03/2016.
 */
@Singleton
@Component(modules = {CommuModule.class})
public interface CommuComponent {
    void inject(MainActivity activity);
}
