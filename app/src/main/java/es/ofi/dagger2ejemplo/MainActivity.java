package es.ofi.dagger2ejemplo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import es.ofi.dagger2ejemplo.commu.CommuFake;
import es.ofi.dagger2ejemplo.commu.CommuServiceRest;
import es.ofi.dagger2ejemplo.model.SegundaActivity;


public class MainActivity extends AppCompatActivity {

    @Inject
    CommuFake _commu;
    TextView user;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication)getApplication()).getCommuComponent().inject(this);
        boolean injected =  _commu == null ? false : true;

        TextView userAvailable = (TextView) findViewById(R.id.target);
        TextView uservalidate = (TextView) findViewById(R.id.uservalidate);
        Button botoon = (Button) findViewById(R.id.clickEjemplo);
        user = (TextView) findViewById(R.id.user);
        userAvailable.setText("Dependency injection worked: " + String.valueOf(injected));
        uservalidate.setText("User Validate: " + _commu.validateUser("aa", "aa"));
        user.setText(_commu.getUsuario().getNombre());

        if(user.getText().toString().equalsIgnoreCase("aaa")) intent = new Intent(this,Test.class);
        if(user.getText().toString().equalsIgnoreCase("Alberto")) intent = new Intent(this,SegundaActivity.class);
        botoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.getText().toString().isEmpty())
                startActivity(intent);
            }
        });
    }

}